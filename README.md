# Lizenztexte



Ein Ziel von openCode ist die Reduktion juristischer Hürden im Umgang mit Open-Source-Software. Eine dieser Hürden sind die Open-Source-Lizenzen selbst, da nicht zu jeder Lizenz unmittelbar ersichtlich ist, ob diese der Open Source Definition entspricht.

Um dem zu begegnen, führt openCode eine Liste von Open-Source-Lizenzen, zu denen eine juristische Prüfung festhalten konnte, ob die Lizenz als Open Source klassifiziert werden kann. Dabei verweist openCode weitgehend auf standardisierte Lizenzen, etwa nach SPDX und Scancode.

Dennoch kommt es vor, dass Lizenzen an openCode herangetragen werden, die, auch unter Berücksichtigung der SPDX Matching Guidelines, noch nicht upstream bekannt sind. Solche Lizenzen werden in diesem Repository gesammelt. Mit der Kennung *Licenseref-OpenCoDE-* ist dieses Repository zu Referenzieren.

## Merge mit etablierten Datenbanken 
Grundsätzlich wird für jede Lizenz der Merge mit etablierten Lizenzdatenbanken (z.B. Scancode) gesucht. Ist eine Lizenz dort eingegangen, wird der Inhalt der Textdatei hier mit "Merged into Licenseref-Scancode-XXX" überschrieben und ist somit unter der openCode-Kennung obsolet.
